import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import appStyles from '../styles/appStyles';
import Grid from 'material-ui/Grid';
import Typography from 'material-ui/Typography';

const TreeListItem = ({ classes, theme, image}) => {
    return (
        <Grid item lg={4} md={4} sm={3} xs={3} className={'treeListItem'}>
            <div>
                <img src={image} width='100%' alt='img'/>
                <Typography align="center">
                Hoa hong
                </Typography>
            </div>
        </Grid>
    );
};

TreeListItem.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
    image: PropTypes.string.isRequired
};

export default withStyles(appStyles, { withTheme: true })(TreeListItem);
