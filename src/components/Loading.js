import React from 'react';
import { CircularProgress } from 'material-ui/Progress';

class Loading extends React.Component {
    render() {
        return (
            <div style={{
                position: 'fixed',
                top:'0',
                left: '0',
                width: '100vw',
                height: '100vh',
                backgroundColor: '#000000',
                opacity: 0.5,
                zIndex: 1000,
            }}>
                <CircularProgress style={{
                    position: 'fixed',
                    top:'35%',
                    left: '50%',
                    width: '100px',
                    height: '100px',
                    marginTop: '-50px',
                    marginLeft: '-50px'
                }}/>
            </div>
        );
    }
}

export default Loading;
