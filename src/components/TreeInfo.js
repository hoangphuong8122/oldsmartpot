import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import appStyles from '../styles/appStyles';
import Typography from 'material-ui/Typography';
import Chart from './Chart';
import Tabs, {Tab} from 'material-ui/Tabs';
import Hidden from 'material-ui/Hidden';
import IconButton from 'material-ui/IconButton';
import Edit from 'material-ui-icons/Edit';
import { connect } from 'react-redux';
import { selectTab } from '../actions';

const TreeInfo = ({ classes, theme, tabIndex, onSelectTab }) => {
    const humidityGraph = [
        {
            label: 'Độ ẩm hiện tại',
            color: 'blue',
            data: [954, 965, 975, 973, 961, 929, 880, 230, 1024, 459, 497, 554]
        },
        {
            label: 'Độ ẩm an toàn trên',
            color: 'green',
            data: [800, 800, 800, 800, 800, 800, 800, 800, 800, 800, 800, 800]
        },
        {
            label: 'Độ ẩm an toàn trên',
            color: 'green',
            data: [100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100]
        }
    ];

    const temperatureGraph = [
        {
            label: 'Nhiệt độ hiện tại',
            color: 'blue',
            data: [20, 21, 22, 23, 24, 25, 26, 25, 24, 23, 22, 21]
        },
        {
            label: 'Nhiệt độ an toàn trên',
            color: 'green',
            data: [40.5, 40.5, 40.5, 40.5, 40.5, 40.5, 40.5, 40.5, 40.5, 40.5, 40.5, 40.5,]
        },
        {
            label: 'Nhiệt độ an toàn trên',
            color: 'green',
            data: [20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20]
        }
    ];
    return (
        <React.Fragment>
            <Tabs
                value={tabIndex}
                indicatorColor="primary"
                textColor="primary"
                fullWidth
                centered
            >
                <Tab label="Độ ẩm" onClick={()=>onSelectTab(0)}/>
                <Tab label="Nhiệt độ" onClick={()=>onSelectTab(1)}/>
            </Tabs>
            <Typography variant="headline" align="center" style={{marginTop: 15}}>
                Cẩm nhung
                <sup title="chỉnh sửa">
                    <IconButton aria-label="edit">
                        <Edit />
                    </IconButton>
                </sup>
            </Typography>
            <Typography paragraph className={classes.treeInfo}>
                Độ ẩm an toàn: (100.0, 800.0)
            </Typography>
            <Typography paragraph className={classes.treeInfo}>
                Độ ẩm hiện tại: 583.0
            </Typography>
            <Typography paragraph className={classes.treeInfo}>
                Tình trạng thiết bị: Hoạt động
            </Typography>
            <Hidden mdUp>
                {
                    tabIndex === 0 ?
                        <Chart height={220} graph={humidityGraph} minValue={0} maxValue={1200}/> :
                        <Chart height={220} graph={temperatureGraph} minValue={0} maxValue={50}/>
                }
            </Hidden>
            <Hidden smDown>
                {
                    tabIndex === 0 ?
                        <Chart graph={humidityGraph} minValue={0} maxValue={1200}/> :
                        <Chart graph={temperatureGraph} minValue={0} maxValue={50}/>
                }
            </Hidden>
        </React.Fragment>
    );
};

TreeInfo.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
    tabIndex: PropTypes.number.isRequired,
    onSelectTab : PropTypes.func.isRequired
};

const mapStateToProps = (state, ownProps) => {
    return {
        tabIndex: state.tabIndex
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onSelectTab: (tabIndex) => {
            dispatch(selectTab(tabIndex));
        }
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(appStyles, { withTheme: true })(TreeInfo));
