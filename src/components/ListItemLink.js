import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

import { withStyles } from 'material-ui/styles';
import {ListItem, ListItemText} from 'material-ui/List';
import appStyles from '../styles/appStyles';

class ListItemLink extends React.Component {
    render() {
        let { to, children, icon, classes } = this.props;
        return (
            <NavLink exact to={to} activeClassName={classes.activeLink} className={classes.link} >
                <ListItem button>
                    {icon}
                    <ListItemText primary={children} disableTypography/>

                </ListItem>
            </NavLink>
        );
    }
}

ListItemLink.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
    to: PropTypes.string.isRequired,
    children: PropTypes.string.isRequired,
    icon: PropTypes.object
};

export default withStyles(appStyles, { withTheme: true })(ListItemLink);
