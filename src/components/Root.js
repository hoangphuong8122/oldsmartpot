import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import '../index.css';
import Login from './Login';
import App from './App';
import Loading from './Loading';

class Root extends React.Component {
    render() {
        let { isAuthenticated, isFetching } = this.props;
        return (
            <React.Fragment>
                { isAuthenticated === null
                    ? null
                    : ( isAuthenticated === true
                        ? <App />
                        : <Login />
                    )
                }
                { isFetching ? <Loading /> : null}
            </React.Fragment>
        );
    }
}

Root.propTypes = {
    isAuthenticated: PropTypes.bool.isRequired,
    isFetching: PropTypes.bool.isRequired
};

const mapStateToProps = (state, ownProps) => {
    return {
        isAuthenticated: state.isAuthenticated,
        isFetching: state.isFetching
    };
};


export default connect(mapStateToProps,undefined)(Root);
