import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { withStyles } from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import IconButton from 'material-ui/IconButton';
import MenuIcon from 'material-ui-icons/Menu';

import { openMobileMenu } from '../actions';
import appStyles from '../styles/appStyles';

class MyAppBar extends React.Component {
    onOpenMenu = () => {
        this.props.dispatch(openMobileMenu());
    }

    render() {
        let { classes } = this.props;
        return (
            <AppBar className={classes.appBar}>
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={this.onOpenMenu}
                        className={classes.navIconHide}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography
                        variant="title"
                        color="inherit"
                        className={classes.flex}
                        align="center"
                        noWrap
                    >
                        SmartPot
                    </Typography>
                </Toolbar>
            </AppBar>
        );
    }
}

MyAppBar.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired
};


export default connect()(withStyles(appStyles, { withTheme: true })(MyAppBar));
