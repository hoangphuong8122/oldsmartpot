import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { withStyles } from 'material-ui/styles';
import Divider from 'material-ui/Divider';
import List, {ListItem, ListItemText} from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import Typography from 'material-ui/Typography';

import ListItemLink from './ListItemLink';
import appStyles from '../styles/appStyles';
import { closeMobileMenu } from '../actions';
import Home from 'material-ui-icons/Home';
import Toys from 'material-ui-icons/Toys';
import Store from 'material-ui-icons/Store';
import History from 'material-ui-icons/History';
import Info from 'material-ui-icons/Info';
import VpnKey from 'material-ui-icons/VpnKey';
import ExitToApp from 'material-ui-icons/ExitToApp';
import { authenticate } from '../actions/authentication';

class DrawerContent extends React.Component {
    onCloseMobileMenu = () => {
        this.props.dispatch(closeMobileMenu());
    }

    render() {
        let { classes, dispatch } = this.props;
        return (
            <div className={classes.drawer}>
                <div className={classes.drawerHeader} >
                    <br />
                    <Avatar className={classes.bigAvatar}>
                        S
                    </Avatar>
                    <br />
                    <Typography align="center" variant="headline" className={classes.drawerText}>
                        Le Minh Son
                    </Typography>
                    <br />
                </div>
                <List onClick={this.onCloseMobileMenu}>
                    <ListItemLink to='/' icon={<Home/>}>Home</ListItemLink>
                    <ListItemLink to='/blogs' icon={<Toys/>}>Bí quyết chăm sóc cây</ListItemLink>
                    <ListItemLink to='/devices' icon={<Store/>}>Kho thiết bị của bạn</ListItemLink>
                    <ListItemLink to='/notifications' icon={<History/>}>Lịch sử cảnh báo</ListItemLink>
                    <ListItemLink to='/about' icon={<Info/>}>Giới thiệu sản phẩm</ListItemLink>
                </List>
                <Divider />
                <List onClick={this.onCloseMobileMenu}>
                    <ListItem button>
                        <VpnKey/>
                        <ListItemText primary='Đổi mật khẩu' disableTypography/>
                    </ListItem>
                    <ListItem button>
                        <ExitToApp/>
                        <ListItemText
                            primary='Đăng xuất'
                            disableTypography
                            onClick = {()=>{
                                localStorage.removeItem('token');
                                dispatch(authenticate(false));
                            }}
                        />
                    </ListItem>
                </List>
                <Divider />
            </div>
        );
    }
}

DrawerContent.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired
};

export default connect(
    null,
    null,
    null,
    {pure: false}
)(withStyles(appStyles, { withTheme: true })(DrawerContent));
