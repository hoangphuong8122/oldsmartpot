import React from 'react';
import { Line } from 'react-chartjs-2';
import 'chartjs-plugin-datalabels';

const createDataset = ({label, color, data}) => {
    return {
        label: label,
        fill: false,
        lineTension: 0.1,
        backgroundColor: color,
        borderColor: color,
        data: data
    };
};
const Chart = ({ height, graph, minValue, maxValue }) => {
    let chartData = {
        labels: [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22],
        datasets: graph.map(element => createDataset(element))
    };

    let options = {
        // showAllTooltips: true,
        plugins: {
            datalabels: {
                display: true,
                color: 'black',
                // anchor: 'end',
                align: 'end',
                offset: -3
            }
        },
        scales: {
            yAxes: [
                {
                    ticks: {
                        beginAtZero: true,
                        min: minValue,
                        max: maxValue
                    }
                }
            ]
        },
        // responsive : true,
    };
    return (
        <Line data={chartData} options={options} height={height}/>
    );
};

export default Chart;
