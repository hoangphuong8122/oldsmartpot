import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { withStyles } from 'material-ui/styles';
import Drawer from 'material-ui/Drawer';
import Hidden from 'material-ui/Hidden';

import DrawerContent from './DrawerContent';
import { closeMobileMenu } from '../actions';
import appStyles from '../styles/appStyles';

class ResponsiveDrawer extends React.Component {
    onCloseMobileMenu = () => {
        this.props.dispatch(closeMobileMenu());
    }

    render() {
        let { classes, theme, open } = this.props;
        return (
            <React.Fragment>
                <Hidden mdUp>
                    <Drawer
                        variant="temporary"
                        anchor={theme.direction === 'rtl' ? 'right' : 'left'}
                        open={open}
                        classes={{
                            paper: classes.drawerPaper,
                        }}
                        onClose={this.onCloseMobileMenu}
                        ModalProps={{
                            keepMounted: true,
                        }}
                    >
                        <DrawerContent />
                    </Drawer>
                </Hidden>
                <Hidden smDown implementation="css">
                    <Drawer
                        variant="permanent"
                        open
                        classes={{
                            paper: classes.drawerPaper,
                        }}
                    >
                        <DrawerContent />
                    </Drawer>
                </Hidden>
            </React.Fragment>
        );
    }
}

ResponsiveDrawer.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
    open: PropTypes.bool.isRequired,
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = (state, ownProps) => {
    return {
        open: state.mobileMenuOpen
    };
};

export default connect(
    mapStateToProps,
    null,
    null,
    {pure: false}
)(withStyles(appStyles, { withTheme: true })(ResponsiveDrawer));
