import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import appStyles from '../styles/appStyles';
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import TreeListItem from './TreeListItem';
const TreeList = ({ classes, theme}) => {
    return (
        <React.Fragment>
            <Paper className={classes.panel}>
                <Grid container>
                    <TreeListItem image='/happy.png' selected/>
                    <TreeListItem image='/sad.png'/>
                    <TreeListItem image='/happy.png'/>
                    <TreeListItem image='/sad.png'/>



                </Grid>
            </Paper>
        </React.Fragment>
    );
};

TreeList.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

export default withStyles(appStyles, { withTheme: true })(TreeList);
