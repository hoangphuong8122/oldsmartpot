import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import appStyles from '../styles/appStyles';
import Grid from 'material-ui/Grid';
import TreeInfo from './TreeInfo';
import TreeList from './TreeList';
import { Route } from 'react-router-dom';

class Blogs extends React.Component {
    render() {
        return (
            <div>
                <h2>Blogs</h2>
            </div>
        );
    }
}

class Devices extends React.Component {
    render() {
        return (
            <div>
                <h2>Devices</h2>
            </div>
        );
    }
}

const Main = ({ classes, theme}) => {
    return (
        <Grid container className={classes.content}>
            <Grid item lg={8} md={8} sm={12} xs={12}>
                <React.Fragment>
                    <Route exact path='/' component={TreeInfo}></Route>
                    <Route exact path='/blogs' component={Blogs}></Route>
                    <Route exact path='/devices' component={Devices}></Route>
                </React.Fragment>
            </Grid>
            <Grid item lg={4} md={4} sm={12} xs={12}>
                <TreeList />
            </Grid>
        </Grid>
    );
};

Main.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

export default withStyles(appStyles, { withTheme: true })(Main);
