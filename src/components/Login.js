import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import $ from 'jquery';

import { withStyles } from 'material-ui/styles';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import Typography from 'material-ui/Typography';
import Snackbar from 'material-ui/Snackbar';
import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui-icons/Close';
import Dialog, {
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
} from 'material-ui/Dialog';

import { userAPI } from '../apiConfig';
import { authenticate } from '../actions/authentication';
import { startFeching, finishFetching } from '../actions/dataFetching';
import loginStyles from '../styles/loginStyles';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            email: '',
            usernameError: false,
            passwordError: false,
            showSnackbar: false,
            snackbarMessage: '',
            showDialog: false
        };
    }

    handleChange = (e) => {
        let { name, value } = e.target;
        this.setState({
            [name]: value,
            [`${name}Error`]: (value === '')
        });
    }

    handleLogin = (e) => {
        e.preventDefault();
        let { username, password } = this.state;
        let { dispatch } = this.props;
        if (username === '' || password === '') {
            this.setState({
                usernameError: (username === ''),
                passwordError: (password === '')
            });
            this.openSnackbar('Thiếu thông tin');
        } else {
            dispatch(startFeching());
            var settings = {
                'async': true,
                'crossDomain': true,
                'url': userAPI.logIn,
                'method': 'POST',
                'headers': {
                    'Content-Type': 'application/json',
                },
                'data': JSON.stringify({username, password})
            };

            $.ajax(settings)
                .done((response)=> {
                    localStorage.setItem('token', response.key);
                    dispatch(authenticate(true));
                })
                .fail(() => {
                    this.openSnackbar('Sai tên đăng nhập hoặc mật khẩu');
                    this.setState({
                        usernameError: true,
                        passwordError: true
                    });
                })
                .always(()=> {
                    dispatch(finishFetching());
                });
        }
    }

    openSnackbar = (message) => {
        this.setState({
            showSnackbar: true,
            snackbarMessage: message
        });
    }
    closeSnackbar = () => {
        this.setState({
            showSnackbar: false,
            snackbarMessage: ''
        });
    }

    showDialog = () => {
        this.setState({
            showDialog: true
        });
    }

    closeDialog = () => {
        this.setState({
            showDialog: false
        });
    }

    handleForgetPassword = () => {
        //this.state.email
    }

    render() {
        let { classes } = this.props;
        let { username,
            password,
            usernameError,
            passwordError,
            showSnackbar,
            snackbarMessage,
            showDialog
        } = this.state;

        return (
            <React.Fragment>
                <div className={classes.container}>
                    <div className={classes.noContent}>
                        <Typography variant="headline" align="center" className={classes.title}>
                            SmartPot
                        </Typography>
                    </div>
                    <Paper className={classes.loginPanel}>
                        <Typography variant="headline" align="center" color="primary">
                            Đăng nhập
                        </Typography>
                        <form>
                            <TextField
                                label="Username"
                                placeholder="Username"
                                name="username"
                                className={classes.textField}
                                margin="normal"
                                autoFocus
                                onChange={this.handleChange}
                                error={usernameError}
                                value={username}
                            />
                            <TextField
                                label="Password"
                                name="password"
                                className={classes.textField}
                                type="password"
                                margin="normal"
                                onChange={this.handleChange}
                                error={passwordError}
                                value={password}
                            />
                            <br />
                            <br />
                            <Button
                                variant="raised"
                                color="primary"
                                type="submit"
                                onClick={this.handleLogin}
                            >
                                Đăng nhập
                            </Button>
                            <br />
                            <br />
                            <Button
                                color="primary"
                                size="small"
                                onClick={this.showDialog}
                                style={{textTransform: 'capitalize'}}>
                                Quên mật khẩu
                            </Button>
                        </form>
                    </Paper>
                </div>
                <Snackbar
                    open={showSnackbar}
                    autoHideDuration={3000}
                    onClose={this.closeSnackbar}
                    message={<span id="message-id">{snackbarMessage}</span>}
                    action={
                        <IconButton color="inherit" onClick={this.closeSnackbar}>
                            <CloseIcon />
                        </IconButton>
                    }
                />
                <Dialog
                    open={showDialog}
                    onClose={this.closeDialog}
                >
                    <DialogTitle>Lấy lại mật khẩu</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Nhập email để lấy lại mật khẩu. Hệ thống sẽ gửi lại mật khẩu mới cho bạn.
                        </DialogContentText>
                        <TextField
                            autoFocus
                            label="Email"
                            type="email"
                            name="email"
                            fullWidth
                            onChange={this.handleChange}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleForgetPassword} color="primary">
                            Gửi
                        </Button>
                        <Button onClick={this.closeDialog} color="primary">
                            Đóng
                        </Button>
                    </DialogActions>
                </Dialog>
            </React.Fragment>
        );
    }
}

Login.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired
};

export default connect()(withStyles(loginStyles, { withTheme: true })(Login));
