import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from 'material-ui/styles';

import ResponsiveDrawer from './ResponsiveDrawer';
import MyAppBar from './MyAppBar';
import Main from './Main';
import appStyles from '../styles/appStyles';
import { BrowserRouter as Router } from 'react-router-dom';

class App extends React.Component {
    render() {
        let { classes } = this.props;
        return (
            <div className={classes.app}>
                <Router>
                    <div className={classes.appFrame}>
                        <MyAppBar />
                        <ResponsiveDrawer />
                        <Main />
                    </div>
                </Router>
            </div>
        );
    }
}

App.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

export default withStyles(appStyles, { withTheme: true })(App);
