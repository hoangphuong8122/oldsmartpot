import React from 'react';
import { Navbar, Nav, NavItem, NavDropdown, MenuItem, Glyphicon } from 'react-bootstrap';

const Header = () => {
    let userDropdownTitle = <Glyphicon glyph="user"></Glyphicon>;
    return (
        <Navbar>
            <Navbar.Header>
                <Navbar.Brand>
                    <a href="/">SmartPot</a>
                </Navbar.Brand>
                <Navbar.Toggle />
            </Navbar.Header>
            <Navbar.Collapse>
                <Nav pullRight>
                    <NavDropdown title={userDropdownTitle} id="basic-nav-dropdown">
                        <MenuItem>Đổi mật khẩu</MenuItem>
                        <MenuItem>Đăng xuất</MenuItem>
                    </NavDropdown>
                </Nav>
                <Nav divider />
                <Nav>
                    <NavItem active>
                        Home
                    </NavItem>
                    <NavItem>
                        Bí quyết chăm sóc cây
                    </NavItem>
                    <NavItem>
                        Kho thiết bị
                    </NavItem>
                    <NavItem>
                        Lịch sử cảnh báo
                    </NavItem>
                    <NavItem>
                        About
                    </NavItem>
                </Nav>
            </Navbar.Collapse>
        </Navbar>

    //       {/* <Navbar>
    //   <Navbar.Header>
    //     <Navbar.Brand>
    //       <a href="#">React-Bootstrap</a>
    //     </Navbar.Brand>
    //     <Navbar.Toggle />
    //   </Navbar.Header>
    //   <Navbar.Collapse>
    //     <Nav>
    //       <NavItem eventKey={1} href="#">Link</NavItem>
    //       <NavItem eventKey={2} href="#">Link</NavItem>
    //       <NavDropdown eventKey={3} title="Dropdown" id="basic-nav-dropdown">
    //         <MenuItem eventKey={3.1}>Action</MenuItem>
    //         <MenuItem eventKey={3.2}>Another action</MenuItem>
    //         <MenuItem eventKey={3.3}>Something else here</MenuItem>
    //         <MenuItem divider />
    //         <MenuItem eventKey={3.3}>Separated link</MenuItem>
    //       </NavDropdown>
    //     </Nav>
    //     <Nav pullRight>
    //       <NavItem eventKey={1} href="#">Link Right</NavItem>
    //       <NavItem eventKey={2} href="#">Link Right</NavItem>
    //     </Nav>
    //   </Navbar.Collapse>
    // </Navbar> */}
    );
};

export default Header;
