export const START_FETCHING = 'START_FETCHING';
export const startFeching = () =>({
    type: START_FETCHING
});

export const FINISH_FETCHING = 'FINISH_FETCHING';
export const finishFetching = () => ({
    type: FINISH_FETCHING
});
