export const actionType = {
    OPEN_MOBILE_MENU: 'OPEN_MOBILE_MENU',
    CLOSE_MOBILE_MENU: 'CLOSE_MOBILE_MENU',
    SELECT_TAB: 'SELECT_TAB',
};

export const openMobileMenu = () => ({
    type: actionType.OPEN_MOBILE_MENU
});

export const closeMobileMenu = () => ({
    type: actionType.CLOSE_MOBILE_MENU
});

export const selectTab = (tabIndex) => ({
    type: actionType.SELECT_TAB,
    tabIndex: tabIndex
});
