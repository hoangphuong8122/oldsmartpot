export const AUTHENTICATE = 'AUTHENTICATE';
export const authenticate = (isAuthenticated) =>({
    type: AUTHENTICATE,
    isAuthenticated
});

export const GET_USER = 'GET_USER';
export const getUser = (user) => ({
    type: GET_USER,
    user
});
