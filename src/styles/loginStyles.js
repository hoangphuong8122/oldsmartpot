const loginStyles = theme => ({
    container: {
        height: '100vh',
        width: '100vw',
        backgroundColor: 'rgba(0, 0, 0, 0.1)',
        margin: 0
    },
    noContent: {
        width: '100%',
        height: '40%',
        backgroundColor:  theme.palette.primary.main,
    },
    title: {
        fontWeight: 'bold',
        fontSize: '3em',
        color: 'white'
    },
    loginPanel: {
        position:'fixed',
        textAlign: 'center',
        overflowY: 'auto',
        paddingTop: '1em',
        [theme.breakpoints.up('sm')]: {
            top: '40%',
            left: '50%',
            width:'30em',
            height:'18em',
            marginTop: '-9em',
            marginLeft: '-15em',
        },
        [theme.breakpoints.down('xs')]: {
            top: '40%',
            left: '50%',
            width:'90%',
            height:'18em',
            marginLeft: '-45%',
            marginTop: '-5em',
        },
    },
    navigation: {
        textAlign: 'center',
        position: 'absolute',
        bottom: '10%'
    },
    textField: {
        width: '80%',
    },
    flatButton: {
        '&:hover': {
            cursor: 'pointer'
        }
    }
});

export default loginStyles;
