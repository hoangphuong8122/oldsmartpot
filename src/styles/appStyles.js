const drawerWidth = 260;

const appStyles = theme => ({
    flex : {
        flex: 1
    },

    //App
    app: {
        width: '100vw',
        height: '100vh',
        zIndex: 1,
        overflow: 'hidden',
    },
    appFrame: {
        position: 'relative',
        display: 'flex',
        width: '100%',
        height: '100%',
        overflow: 'hidden',
    },

    //AppBar
    appBar: {
        position: 'fixed',
        marginLeft: drawerWidth,
        [theme.breakpoints.up('md')]: {
            width: `calc(100% - ${drawerWidth}px)`,
        },
    },
    navIconHide: {
        [theme.breakpoints.up('md')]: {
            display: 'none',
        },
    },

    //Drawer
    drawer: {
        [theme.breakpoints.up('md')]: {
            height: '100vh',
        },
        [theme.breakpoints.down('sm')]: {
            marginBottom: '80px',
        },
    },
    drawerHeader: {
        marginTop: 0,
        backgroundImage: 'url(\'/background.jpg\')',
        backgroundSize: 'cover',
    },
    bigAvatar: {
        margin: 'auto',
        width: 80,
        height: 80,
        fontSize: 50
    },
    drawerText: {
        color: 'white'
    },
    drawerPaper: {
        width: drawerWidth + 10,
        [theme.breakpoints.up('md')]: {
            width: drawerWidth,
            position: 'relative',
            height: '100%',
        },
    },

    link: {
        color: 'black',
        textDecoration: 'none'
    },
    activeLink: {
        color: 'green',
        borderLeft: '5px solid green',
        display: 'block'
    },

    content: {
        backgroundColor: theme.palette.background.default,
        width: '100%',
        paddingLeft: '1%',
        paddingRight: '1%',
        // height: 'calc(100vh - 56px)',
        marginTop: 56,
        [theme.breakpoints.up('sm')]: {
            // height: 'calc(100vh - 64px)',
            marginTop: 64,
        },
        overflow: 'scroll'
    },



    treeInfo: {
        marginBottom: '1%',
        paddingLeft: '10%',
    },

    card: {
        width: 150,
        display: 'inline-block',
        margin: 5
    },
    media: {
        height: 150,
        backgroundSize: 'cover'
    },
    panel: {
        marginTop: 10,
        padding: 10,
        backgroundColor: theme.palette.primary.main,
        height: '70vh',
        overflow: 'scroll'
    },
});

export default appStyles;
