import { AUTHENTICATE } from '../actions/authentication';

export const isAuthenticated = (state = false, action) => {
    switch(action.type) {
        case AUTHENTICATE:
            return action.isAuthenticated;
        default:
            return state;
    }
};
