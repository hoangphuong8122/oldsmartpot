import { START_FETCHING, FINISH_FETCHING } from '../actions/dataFetching';

export const isFetching = (state = false, action) => {
    switch(action.type) {
        case START_FETCHING:
            return true;
        case FINISH_FETCHING:
            return false;
        default:
            return state;
    }
};
