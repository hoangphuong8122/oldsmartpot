import { LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE } from '../actions/login';

const getInitialState = () => {
    return {

    };
};
export const user = (state = getInitialState(), action) => {
    switch (action.type) {
        case LOGIN_REQUEST:
            return {
                isFetching: true,
                isAuthenticated: false
            };
        case LOGIN_SUCCESS:
            return {
                isFetching: false,
                isAuthenticated: true,
                token: action.token
            };
        case LOGIN_FAILURE:
            return {
                isFetching: false,
                isAuthenticated: false,
            };
        // case LOGOUT_SUCCESS:
        //     return {
        //         isFetching: true,
        //         isAuthenticated: false
        //     }
        default:
            return state;
    }
};
