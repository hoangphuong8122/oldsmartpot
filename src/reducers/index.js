import { combineReducers } from 'redux';
import { mobileMenuOpen } from './mobileMenuOpen';
import { tabIndex } from './tabIndex';
import { user } from './user';
import { isAuthenticated } from './isAuthenticated';
import { isFetching } from './isFetchingData';

export default combineReducers({
    isAuthenticated,
    user,
    isFetching,
    mobileMenuOpen,
    tabIndex,
});
