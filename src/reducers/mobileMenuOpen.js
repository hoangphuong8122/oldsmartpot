import { actionType } from '../actions';

export const mobileMenuOpen = (state = false, action) => {
    switch (action.type) {
        case actionType.OPEN_MOBILE_MENU:
            return true;
        case actionType.CLOSE_MOBILE_MENU:
            return false;
        default:
            return state;
    }
};
