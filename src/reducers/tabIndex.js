import { actionType } from '../actions';

export const tabIndex = (state = 0, action) => {
    switch (action.type) {
        case actionType.SELECT_TAB:
            return action.tabIndex;
        default:
            return state;
    }
};
