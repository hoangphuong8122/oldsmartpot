import React from 'react';
import ReactDOM from 'react-dom';
import { createMuiTheme, MuiThemeProvider } from 'material-ui/styles';
import * as Colors from 'material-ui/colors';
import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { Provider } from 'react-redux';
import $ from 'jquery';

import Root from './components/Root';
import rootReducer from './reducers';
import { authenticate, getUser } from './actions/authentication';
import { userAPI } from './apiConfig';
import { startFeching, finishFetching } from './actions/dataFetching';

const theme = createMuiTheme({
    palette: {
        primary: Colors.green
    }
});

const store = createStore(
    rootReducer,
    applyMiddleware(thunkMiddleware)
);
document.store = store;

/* make an ajax call to verify wether the TOKEN is valid if it exists */
if(localStorage.hasOwnProperty('token')) {
    store.dispatch(startFeching());
    var settings = {
        'async': false,
        'crossDomain': true,
        'url': userAPI.getUser,
        'method': 'GET',
        'headers': {
            'Authorization': 'Token ' + localStorage.getItem('token'),
        }
    };

    $.ajax(settings)
        .done((response) => {
            store.dispatch(authenticate(true));
            store.dispatch(getUser(response));
        })
        .fail(() => {
            store.dispatch(authenticate(false));
        })
        .always(()=> {
            store.dispatch(finishFetching());
        });
} else {
    store.dispatch(authenticate(false));
}

ReactDOM.render(
    <MuiThemeProvider theme={theme}>
        <Provider store={store}>
            <Root />
        </Provider>
    </MuiThemeProvider>,
    document.getElementById('root')
);
