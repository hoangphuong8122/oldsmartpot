const baseUrl = 'https://visd.herokuapp.com/api/v1/';

export const userAPI = {
    logIn: baseUrl + 'auth/login/',
    logOut: baseUrl + 'auth/logout/',
    getUser: baseUrl + 'auth/user/'
};

export const devicesAPI = {
    getDevices: baseUrl + 'devices/'
};

export const notificationAPI = {
    getNotifications: baseUrl + 'notifications/'
};

export const smartpotAPU = {
    getSmartPots: baseUrl + 'smart_pot'
};
